//
//  RecentMediaController.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

protocol RecentMediaControllerProtocol {

    func photoList(handler: @escaping (Response<Photos, ResponseError>) -> ())
}

class RecentMediaController: RecentMediaControllerProtocol {

    fileprivate let connection: RequestConnectionProtocol
    fileprivate let authenticationController: AuthenticationControllerProtocol

    // MARK: - Public interface

    init (connection: RequestConnectionProtocol = RequestConnection(), authenticationController: AuthenticationControllerProtocol = AuthenticationController()) {
        self.connection = connection
        self.authenticationController = authenticationController
    }

    func photoList(handler: @escaping (Response<Photos, ResponseError>) -> ()) {

        guard let token = authenticationController.getToken() else { return }
        let request = RecentMediaRequest(token: token)
        connection.performRequest(request: request) { (response) in

            switch response {
            case .success(let photos):
                handler(.success(photos))
            case .error(let error):
                handler(.error(error))
            }
        }
    }
}
