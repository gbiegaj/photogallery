//
//  AuthenticationController.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

protocol AuthenticationControllerProtocol {

    func storeToken(token: String)
    func getToken() -> String?
}

class AuthenticationController: AuthenticationControllerProtocol {

    fileprivate let keychainStorage: KeychainStorageProtocol

    // MARK: - Public interface
    
    init (keychainStorage: KeychainStorageProtocol = KeychainStorage()) {
        self.keychainStorage = keychainStorage
    }
    
    func storeToken(token: String) {
        let authentication = Authentication(token: token)
        keychainStorage.tryStore(model: authentication)
    }

    func getToken() -> String? {
        guard let authentication: Authentication = keychainStorage.tryRestore() else { return nil }
        return authentication.token
    }
}
