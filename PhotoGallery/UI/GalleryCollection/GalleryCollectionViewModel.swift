//
//  GalleryCollectionViewModel.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

class GalleryCollectionViewModel {

    fileprivate var recentMediaController: RecentMediaControllerProtocol

    // MARK: - Public interface
    
    init (recentMediaController: RecentMediaControllerProtocol = RecentMediaController()) {
        self.recentMediaController = recentMediaController
    }

    var photos: [Photo] = []

    func getData(handler: @escaping (Response<[Photo], ResponseError>) -> ()) {
        recentMediaController.photoList { response in
            switch response {
            case .success(let photos):
                self.photos = photos.data
                handler(Response.success(self.photos))
            case .error(let error):
                handler(Response.error(error))
            }
        }
    }

}
