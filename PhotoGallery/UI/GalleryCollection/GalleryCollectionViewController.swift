//
//  GalleryCollectionViewController.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import UIKit

class GalleryCollectionViewController: UIViewController {

    fileprivate var currentPhotoIndex: Int?

    fileprivate var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        return refreshControl
    }()

    // MARK: - View model

    fileprivate let vm = GalleryCollectionViewModel()

    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.backgroundView = backgroundCollectionView
            if #available(iOS 10.0, *) {
                collectionView.refreshControl = refreshControl
            } else {
                collectionView.addSubview(refreshControl)
                collectionView.alwaysBounceVertical = true
            }
        }
    }
    @IBOutlet var backgroundCollectionView: UIView!
    @IBOutlet weak var backgroundLabel: UILabel! {
        didSet {
            backgroundLabel.text = "no_photos".localized
        }
    }
    @IBOutlet weak var logoutButton: UIBarButtonItem! {
        didSet {
           logoutButton.title = "logout_button".localized
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: - Actions

    @IBAction func onLogoutTap(_ sender: UIBarButtonItem) {
        showLogoutQuestion()
    }

    // MARK: - VC Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "instagram_photos_header".localized
        getData()
    }

    // MARK: - Private

    @objc fileprivate func refreshWeatherData(_ sender: Any) {
        getData()
    }

    fileprivate func getData() {

        backgroundLabel.isHidden = true
        activityIndicator.startAnimating()

        vm.getData { response in
            self.refreshControl.endRefreshing()
            switch response {
            case .success(let photos):
                self.backgroundLabel.isHidden = false
                self.activityIndicator.stopAnimating()
                self.collectionView.backgroundView = photos.count == 0 ? self.backgroundCollectionView : nil
                self.collectionView.reloadData()
            case .error(let error):
                self.activityIndicator.stopAnimating()
                self.showAlert(error: error)
            }
        }
    }

    fileprivate func showAlert(error: ResponseError) {
        showAlert(withTitle:"connection_message_title".localized,
                  message: "connection_message".localized + error.errorDescription,
                  confirmButtonTitle: "retry_button".localized,
                  cancelButtonTitle: "cancel_button".localized,
                  confirmAction: { action in
                    self.getData()
        })
    }

    fileprivate func showLogoutQuestion() {
        showAlert(withTitle:"connection_message_title".localized,
                  message: "logout_question".localized,
                  confirmButtonTitle: "yes_button".localized,
                  cancelButtonTitle: "no_button".localized,
                  confirmAction: { action in
                    self.performSegue(withIdentifier: "LogoutSegue", sender: self)
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PictureViewController, segue.identifier == "showPicture" {
            viewController.photos = vm.photos
            viewController.currentPhotoIndex = currentPhotoIndex
        }
    }

}

// MARK: - collection view data source

extension GalleryCollectionViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vm.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageViewCellRI", for: indexPath)
        if let cell = cell as? ImageViewCell {
            cell.setData(photo: vm.photos[indexPath.row])
        }
        return cell
    }

}

// MARK: - collection view data delegate

extension GalleryCollectionViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentPhotoIndex = indexPath.row
        performSegue(withIdentifier: "showPicture", sender: self)
    }
}

// MARK: - collection view data delegate flow layout

extension GalleryCollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let maxSize = CGFloat(collectionView.bounds.size.width / 3)
        return CGSize(width: maxSize, height: maxSize)
    }

}
