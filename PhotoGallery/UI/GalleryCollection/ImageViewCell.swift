//
//  ImageViewCell.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 25.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import UIKit
import Kingfisher

class ImageViewCell: UICollectionViewCell {

    // MARK: - Outlets

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!

    // MARK: - Public interface

    func setData(photo: Photo) {
        let url = URL(string: photo.images.low_resolution.url)
        imageView.kf.setImage(with: url)
        commentLabel.attributedText = NSMutableAttributedString(string: "❤️\(photo.likes.count)  📋\(photo.comments.count)", attributes: strokeTextAttributes)
    }

    // MARK: - Private

    fileprivate let strokeTextAttributes = [
        NSAttributedStringKey.strokeColor: UIColor.black,
        NSAttributedStringKey.foregroundColor: UIColor.white,
        NSAttributedStringKey.strokeWidth: -6.0,
        NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12)]
        as [NSAttributedStringKey: Any]
}
