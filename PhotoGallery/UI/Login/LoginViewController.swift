//
//  ViewController.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 17.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - View model, controllers

    fileprivate let viewModel = LoginViewModel()
    fileprivate let authenticationController = AuthenticationController()

    // MARK: - Outlets

    @IBOutlet weak var webActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView! {
        didSet {
            webView.delegate = self
        }
    }

    // MARK: - VC Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        loadLoginView()
    }

    @IBAction func logoutAndLoginAgain(_ sender: UIStoryboardSegue) {
        viewModel.removeInstagramCookies()
        loadLoginView()
    }

    // MARK: - Private

    fileprivate func loadLoginView() {

        guard let URLReqest = viewModel.URLReqest else { return }
        webView.loadRequest(URLReqest)
    }

    fileprivate func checkRequestForCallbackURL(request: URLRequest) -> Bool {

        guard let requestURLString = (request.url?.absoluteString) else { return false }
        if requestURLString.hasPrefix(viewModel.redirectURI) {
            guard let range = requestURLString.range(of: "#access_token=") else { return false }
            handleAuth(authToken: String(requestURLString[range.upperBound...]))
            performSegue(withIdentifier: "GalleryCollection", sender: self)
            return false
        }
        return true
    }

    fileprivate func handleAuth(authToken: String) {
        authenticationController.storeToken(token: authToken)
    }

    fileprivate func showAlert() {
        showAlert(withTitle: "no_internet_connection_title".localized,
                  message: "no_internet".localized,
                  confirmButtonTitle: "retry_button".localized,
                  cancelButtonTitle: "cancel_button".localized,
                  confirmAction: { action in
                    self.loadLoginView()
        })
    }
}

// MARK: - UIWebViewDelegate

extension ViewController: UIWebViewDelegate {

    func webView(_ webView: UIWebView, shouldStartLoadWith request:URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        webActivityIndicator.startAnimating()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        webActivityIndicator.stopAnimating()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        guard viewModel.isConnectionError(error: error) else { return }
        webViewDidFinishLoad(webView)
        showAlert()
    }
}
