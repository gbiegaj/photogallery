//
//  LoginViewModel.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 17.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

class LoginViewModel {

    // MARK: - Private definitions

    fileprivate static let instagramDomain = "instagram.com"
    fileprivate static let instagramAuthURL = "https://api.instagram.com/oauth/authorize/"
    fileprivate static let instagramClientID = "9c426c5db595486cac3ea19ef39e6d1e"
    fileprivate static let instagramClientSecret = "72d98d7d592d4400bacd351cc7689679"
    fileprivate static let instagramRedirectURI = "http://example.com"
    fileprivate static let instagramScope = "follower_list+public_content"

    // MARK: - Public interface

    var URLReqest: URLRequest? {
        guard let url = URL(string: authUrl) else { return nil }
        return URLRequest(url: url)
    }

    var redirectURI: String {
        return LoginViewModel.instagramRedirectURI
    }

    func removeInstagramCookies() {
        guard let cookies = HTTPCookieStorage.shared.cookies else { return }
        for cookie in cookies as [HTTPCookie] {
            guard cookie.domain.contains(LoginViewModel.instagramDomain) else { continue }
            HTTPCookieStorage.shared.deleteCookie(cookie)
        }
    }

    func isConnectionError(error: Error) -> Bool {
        let nsError = error as NSError
        return nsError.code == -1009 || nsError.code == -1200
    }

    // MARK: - Private

    fileprivate var authUrl: String {
        return String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True",
                      arguments: [LoginViewModel.instagramAuthURL,
                                  LoginViewModel.instagramClientID,
                                  LoginViewModel.instagramRedirectURI,
                                  LoginViewModel.instagramScope])
    }
}
