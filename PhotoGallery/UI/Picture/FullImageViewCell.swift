//
//  FullImageViewCell.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import UIKit

class FullImageViewCell: UICollectionViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var imageView: UIImageView!

    // MARK: - Public interface
    
    func setData(photo: Photo) {
        let url = URL(string: photo.images.standard_resolution.url)
        imageView.kf.setImage(with: url)
    }
    
}
