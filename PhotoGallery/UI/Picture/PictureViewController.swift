//
//  PictureViewController.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 25.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import UIKit

class PictureViewController: UIViewController {

    // MARK: - Public interface
    
    var currentPhotoIndex: Int?
    var photos: [Photo] = []

    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }

    // MARK: - VC Life cycle
    
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.performBatchUpdates(nil, completion: nil)
        guard let currentPhotoIndex = currentPhotoIndex else { return }
        collectionView.scrollToItem(at: IndexPath(item: currentPhotoIndex, section: 0), at: .left, animated: false)
    }

}

// MARK: - collection view data source

extension PictureViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FullImageViewCellRI", for: indexPath)
        if let cell = cell as? FullImageViewCell {
            cell.setData(photo: photos[indexPath.row])
        }
        return cell
    }

}

// MARK: - collection view data delegate flow layout

extension PictureViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height - 64)
    }
    
}

