//
//  RecentMediaInterpreter.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

class RecentMediaInterpreter: InterpreterProtocol {

    // MARK: - Public interface

    func interpret(data: Data?, response: HTTPURLResponse?, error: Error?, successStatusCode: Int) -> Response<Photos, ResponseError> {

        if let _ = error { return Response.error(ResponseError.connectionError) }
        guard response?.statusCode == successStatusCode else {
            return Response.error(ResponseError.invalidResponseError)
        }

        guard let data = data else { return Response.error(ResponseError.invalidResponseError) }
        guard let response = try? JSONDecoder().decode(Photos.self, from: data) else {
            return Response.error(ResponseError.decodeError)
        }
        return Response.success(Photos(data: response.data.filter { $0.type == "image" } ))
    }
}

