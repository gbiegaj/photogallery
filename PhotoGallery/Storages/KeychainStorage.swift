//
//  KeychainStorage.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation
import Locksmith

protocol KeychainStorable {

    init?(map:[String: AnyObject])
    var keychainMap: [String: AnyObject] { get }
    static var keychainKey: String { get }
}

protocol KeychainStorageProtocol {

    @discardableResult func tryStore<T: KeychainStorable>(model: T) -> Bool
    @discardableResult func tryRestore<T: KeychainStorable>() -> T?
    @discardableResult func tryRemove(key: String) -> Bool
}

class KeychainStorage: KeychainStorageProtocol {

    @discardableResult
    func tryStore<T: KeychainStorable>(model: T) -> Bool {

        if let _ = try? Locksmith.updateData(data: model.keychainMap, forUserAccount: T.keychainKey){
            return true
        }

        return false
    }

    @discardableResult
    func tryRestore<T: KeychainStorable>() -> T? {

        guard let restoredMap = Locksmith.loadDataForUserAccount(userAccount: T.keychainKey) else { return nil }

        return T(map: restoredMap as [String : AnyObject])
    }

    @discardableResult
    func tryRemove(key: String) -> Bool {

        if let _ = try? Locksmith.deleteDataForUserAccount(userAccount: key) {
            return true
        }

        return false
    }
}
