//
//  String+localized.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 27.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import UIKit

extension String {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
