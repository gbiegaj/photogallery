
//
//  Photo.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

struct Image: Codable, Equatable {
    let url: String
}

struct Images: Codable, Equatable {
    let thumbnail: Image
    let low_resolution: Image
    let standard_resolution: Image
}

struct Count: Codable, Equatable {
    let count: Int
}

struct Photo: Codable, Equatable {
    let type: String
    let comments: Count
    let likes: Count
    let images: Images
}

struct Photos: Codable, Equatable {
    let data: [Photo]
}
