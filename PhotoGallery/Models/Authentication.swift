//
//  Authentication.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

struct Authentication {
    var token: String
}

extension Authentication: KeychainStorable {

    init?(map:[String:AnyObject]) {

        guard let token = map["token"] as? String else { return nil }
        self.init(token: token)
    }

    var keychainMap: [String: AnyObject] {
        return ["token": token as AnyObject]
    }

    static var keychainKey: String { return "Authentication" }
}
