//
//  RecentMediaRequest.swift
//  PhotoGallery
//
//  Created by Grzegorz Biegaj on 18.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation

struct RecentMediaRequest: RequestProtocol {

    // MARK: - Request protocol

    let token: String

    var endpoint: String { return "https://api.instagram.com/v1/users/self/media/recent" }

    var requestParameters: RequestParameters {
        return ["access_token": token]
    }

    let interpreter: RecentMediaInterpreter = RecentMediaInterpreter()
}
