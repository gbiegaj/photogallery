//
//  PhotosTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class PhotosTests: XCTestCase {
    
    func testImageEquatable() {
        let image1 = Image(url: "testURL_1")
        let image2 = Image(url: "testURL_1")
        let image3 = Image(url: "testURL_2")

        XCTAssertEqual(image1, image2)
        XCTAssertNotEqual(image1, image3)
    }

    func testImagesEquatable() {
        let image1 = Image(url: "testURL_1")
        let image2 = Image(url: "testURL_1")
        let image3 = Image(url: "testURL_2")

        let images1 = Images(thumbnail: image1, low_resolution: image2, standard_resolution: image3)
        let images2 = Images(thumbnail: image1, low_resolution: image2, standard_resolution: image3)
        let images3 = Images(thumbnail: image3, low_resolution: image1, standard_resolution: image2)

        XCTAssertEqual(images1, images2)
        XCTAssertNotEqual(images1, images3)
    }

    func testCountEquatable() {
        let count1 = Count(count: 1)
        let count2 = Count(count: 1)
        let count3 = Count(count: 100)

        XCTAssertEqual(count1, count2)
        XCTAssertNotEqual(count1, count3)
    }

    func testPhotoEquatable() {
        let count1 = Count(count: 1)
        let count2 = Count(count: 1)
        let count3 = Count(count: 100)

        let image1 = Image(url: "testURL_1")
        let image2 = Image(url: "testURL_1")
        let image3 = Image(url: "testURL_2")

        let photo1 = Photo(type: "test", comments: count1, likes: count2, images: Images(thumbnail: image1, low_resolution: image2, standard_resolution: image3))
        let photo2 = Photo(type: "test", comments: count1, likes: count2, images: Images(thumbnail: image1, low_resolution: image2, standard_resolution: image3))
        let photo3 = Photo(type: "test", comments: count1, likes: count3, images: Images(thumbnail: image3, low_resolution: image2, standard_resolution: image1))

        XCTAssertEqual(photo1, photo2)
        XCTAssertNotEqual(photo1, photo3)
    }

    func testPhotosEquatable() {
        let count1 = Count(count: 1)
        let count2 = Count(count: 1)
        let count3 = Count(count: 100)

        let image1 = Image(url: "testURL_1")
        let image2 = Image(url: "testURL_1")
        let image3 = Image(url: "testURL_2")

        let photo1 = Photo(type: "test", comments: count1, likes: count2, images: Images(thumbnail: image1, low_resolution: image2, standard_resolution: image3))
        let photo2 = Photo(type: "test", comments: count1, likes: count2, images: Images(thumbnail: image1, low_resolution: image2, standard_resolution: image3))
        let photo3 = Photo(type: "test", comments: count1, likes: count3, images: Images(thumbnail: image3, low_resolution: image2, standard_resolution: image1))

        let photos1 = Photos(data: [photo1, photo2, photo3])
        let photos2 = Photos(data: [photo1, photo2, photo3])
        let photos3 = Photos(data: [photo2, photo2, photo1])

        XCTAssertEqual(photos1, photos2)
        XCTAssertNotEqual(photos1, photos3)
    }

}

