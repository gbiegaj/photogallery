//
//  RecentMediaControllerTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class RecentMediaControllerTests: XCTestCase {

    func testSuccessData() {

        var data: Data?
        let response = HTTPURLResponse(url: URL(string: "test")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let photo1 = Photo(type: "image", comments: Count(count: 2), likes: Count(count: 3), images: Images(thumbnail: Image(url: "thumbnail_1"), low_resolution: Image(url: "low_resolution_1"), standard_resolution: Image(url: "standard_resolution_1")))
        let photo2 = Photo(type: "image", comments: Count(count: 1), likes: Count(count: 1), images: Images(thumbnail: Image(url: "thumbnail_2"), low_resolution: Image(url: "low_resolution_2"), standard_resolution: Image(url: "standard_resolution_2")))
        let photo3 = Photo(type: "image", comments: Count(count: 8), likes: Count(count: 8), images: Images(thumbnail: Image(url: "thumbnail_3"), low_resolution: Image(url: "low_resolution3"), standard_resolution: Image(url: "standard_resolution_3")))
        let photos = Photos(data: [photo1, photo2, photo3])

        do {
            data = try JSONEncoder().encode(photos)
        }
        catch {
            XCTFail()
        }
        let connectionMock = NetworkingMock(data: data, error: nil, response: response)
        let authenticationControllerMock = AuthenticationControllerMock()
        let recentMediaController = RecentMediaController(connection: connectionMock, authenticationController: authenticationControllerMock)

        authenticationControllerMock.storeToken(token: "MyToken")

        recentMediaController.photoList { response in
            switch response {
            case .success(let readPhotos):
                XCTAssertEqual(readPhotos, photos)
            case .error(_):
                XCTFail()
            }
        }

    }

    func testBadDataType() {

        var data: Data?
        let response = HTTPURLResponse(url: URL(string: "test")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let photo1 = Photo(type: "video", comments: Count(count: 2), likes: Count(count: 3), images: Images(thumbnail: Image(url: "thumbnail_1"), low_resolution: Image(url: "low_resolution_1"), standard_resolution: Image(url: "standard_resolution_1")))
        let photo2 = Photo(type: "video", comments: Count(count: 1), likes: Count(count: 1), images: Images(thumbnail: Image(url: "thumbnail_2"), low_resolution: Image(url: "low_resolution_2"), standard_resolution: Image(url: "standard_resolution_2")))
        let photo3 = Photo(type: "video", comments: Count(count: 8), likes: Count(count: 8), images: Images(thumbnail: Image(url: "thumbnail_3"), low_resolution: Image(url: "low_resolution3"), standard_resolution: Image(url: "standard_resolution_3")))
        let photos = Photos(data: [photo1, photo2, photo3])

        do {
            data = try JSONEncoder().encode(photos)
        }
        catch {
            XCTFail()
        }
        let connectionMock = NetworkingMock(data: data, error: nil, response: response)
        let authenticationControllerMock = AuthenticationControllerMock()
        let recentMediaController = RecentMediaController(connection: connectionMock, authenticationController: authenticationControllerMock)

        authenticationControllerMock.storeToken(token: "MyToken")

        recentMediaController.photoList { response in
            switch response {
            case .success(let readPhotos):
                XCTAssertEqual(readPhotos, Photos(data: []))
            case .error(_):
                XCTFail()
            }
        }

    }
    
}
