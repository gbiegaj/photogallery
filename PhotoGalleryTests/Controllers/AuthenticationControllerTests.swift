//
//  AuthenticationControllerTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class AuthenticationControllerTests: XCTestCase {
    
    func testStoreRestoreToken() {
        let keychainStorage = KeychainStorageMock()
        let authenticationController = AuthenticationController(keychainStorage: keychainStorage)

        let token = "data"
        authenticationController.storeToken(token: token)
        let readToken = authenticationController.getToken()

        XCTAssertEqual(token, readToken)
    }

    
}
