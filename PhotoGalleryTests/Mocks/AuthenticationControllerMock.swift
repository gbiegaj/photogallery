//
//  AuthenticationControllerMock.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation
@testable import PhotoGallery

class AuthenticationControllerMock: AuthenticationControllerProtocol {

    var token: String = ""

    func storeToken(token: String) {
        self.token = token
    }

    func getToken() -> String? {
        return token
    }

}
