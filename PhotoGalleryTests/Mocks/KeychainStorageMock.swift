//
//  KeychainStorageMock.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation
@testable import PhotoGallery

class KeychainStorageMock: KeychainStorageProtocol {

    var data: Any? = nil

    @discardableResult func tryStore<T: KeychainStorable>(model: T) -> Bool {
        data = model
        return true
    }

    @discardableResult func tryRestore<T: KeychainStorable>() -> T? {
        return data as? T
    }

    @discardableResult func tryRemove(key: String) -> Bool {
        return true
    }
    
}
