//
//  RecentMediaControllerMock.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import Foundation
@testable import PhotoGallery

class RecentMediaControllerMock: RecentMediaControllerProtocol {

    var photos: Photos?

    func photoList(handler: @escaping (Response<Photos, ResponseError>) -> ()) {
        if let photos = photos {
            handler(.success(photos))
        } else {
            handler(.error(ResponseError.connectionError))
        }
    }
}
