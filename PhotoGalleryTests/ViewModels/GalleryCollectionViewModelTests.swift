//
//  GalleryCollectionViewModelTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class GalleryCollectionViewModelTests: XCTestCase {

    func testPhotos() {

        let recentMediaController = RecentMediaControllerMock()
        let galleryCollectionViewModel = GalleryCollectionViewModel(recentMediaController: recentMediaController)

        XCTAssertEqual(galleryCollectionViewModel.photos.count, 0)
        XCTAssertEqual(galleryCollectionViewModel.photos, [])
    }

    func testGetData() {

        let recentMediaController = RecentMediaControllerMock()
        let galleryCollectionViewModel = GalleryCollectionViewModel(recentMediaController: recentMediaController)

        let photo1 = Photo(type: "image", comments: Count(count: 2), likes: Count(count: 3), images: Images(thumbnail: Image(url: "thumbnail_1"), low_resolution: Image(url: "low_resolution_1"), standard_resolution: Image(url: "standard_resolution_1")))
        let photo2 = Photo(type: "image", comments: Count(count: 1), likes: Count(count: 1), images: Images(thumbnail: Image(url: "thumbnail_2"), low_resolution: Image(url: "low_resolution_2"), standard_resolution: Image(url: "standard_resolution_2")))
        let photo3 = Photo(type: "image", comments: Count(count: 8), likes: Count(count: 8), images: Images(thumbnail: Image(url: "thumbnail_3"), low_resolution: Image(url: "low_resolution3"), standard_resolution: Image(url: "standard_resolution_3")))
        let photos = Photos(data: [photo1, photo2, photo3])
        recentMediaController.photos = photos

        galleryCollectionViewModel.getData { response in
            switch response {
            case .success(let outputPhotos):
                XCTAssertEqual(outputPhotos, photos.data)
            case .error(_):
                XCTFail()
            }
        }
    }
    
}
