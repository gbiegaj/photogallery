//
//  LoginViewModelTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class LoginViewModelTests: XCTestCase {
    
    func testURLReqest() {

        let loginViewModel = LoginViewModel()
        let testURL = URL(string: "https://api.instagram.com/oauth/authorize/?client_id=9c426c5db595486cac3ea19ef39e6d1e&redirect_uri=http://example.com&response_type=token&scope=follower_list+public_content&DEBUG=True")
        let testURLReqest = URLRequest(url: testURL!)

        XCTAssertEqual(loginViewModel.URLReqest, testURLReqest)
    }

    func testRedirectURI() {

        let loginViewModel = LoginViewModel()
        let redirectURI = "http://example.com"

        XCTAssertEqual(loginViewModel.redirectURI, redirectURI)
    }

    func testIsConnectionError() {

        let loginViewModel = LoginViewModel()
        XCTAssertTrue(loginViewModel.isConnectionError(error: errorConnection1 as Error))
        XCTAssertTrue(loginViewModel.isConnectionError(error: errorConnection2 as Error))
        XCTAssertFalse(loginViewModel.isConnectionError(error: errorOther as Error))
    }

    let errorConnection1 = NSError(domain: "test", code: -1009, userInfo: nil)
    let errorConnection2 = NSError(domain: "test", code: -1200, userInfo: nil)
    let errorOther = NSError(domain: "test", code: 123, userInfo: nil)

}
