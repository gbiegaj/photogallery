//
//  RecentMediaRequestTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class RecentMediaRequestTests: XCTestCase {
    
    func testRecentMediaRequest() {

        let token = "Test123"
        let recentMediaRequest = RecentMediaRequest(token: token)

        XCTAssertEqual(recentMediaRequest.successStatusCode, 200)
        XCTAssertEqual(recentMediaRequest.httpMethod, .get)
        XCTAssertEqual(recentMediaRequest.endpoint, "https://api.instagram.com/v1/users/self/media/recent")
        XCTAssertEqual(recentMediaRequest.requestParameters?["access_token"] as? String, token)
    }

}
