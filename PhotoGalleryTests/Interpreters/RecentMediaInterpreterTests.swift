//
//  RecentMediaInterpreterTests.swift
//  PhotoGalleryTests
//
//  Created by Grzegorz Biegaj on 26.06.18.
//  Copyright © 2018 Grzegorz Biegaj. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class RecentMediaInterpreterTests: XCTestCase {
    
    func testRecentMediaInterpreterValidData() {

        var data: Data?
        let response = HTTPURLResponse(url: URL(string: "test")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let successStatusCode = 200

        let photo1 = Photo(type: "image", comments: Count(count: 2), likes: Count(count: 3), images: Images(thumbnail: Image(url: "thumbnail_1"), low_resolution: Image(url: "low_resolution_1"), standard_resolution: Image(url: "standard_resolution_1")))
        let photo2 = Photo(type: "image", comments: Count(count: 1), likes: Count(count: 1), images: Images(thumbnail: Image(url: "thumbnail_2"), low_resolution: Image(url: "low_resolution_2"), standard_resolution: Image(url: "standard_resolution_2")))
        let photo3 = Photo(type: "image", comments: Count(count: 8), likes: Count(count: 8), images: Images(thumbnail: Image(url: "thumbnail_3"), low_resolution: Image(url: "low_resolution3"), standard_resolution: Image(url: "standard_resolution_3")))
        let photos = Photos(data: [photo1, photo2, photo3])

        do {
            data = try JSONEncoder().encode(photos)
        }
        catch {
            XCTFail()
        }

        let recentMediaInterpreter = RecentMediaInterpreter()
        let resp = recentMediaInterpreter.interpret(data: data, response: response, error: nil, successStatusCode: successStatusCode)
        switch resp {
        case .success(let data):
            XCTAssertEqual(data, photos)
        case .error(_):
            XCTFail()
        }
    }

    func testRecentMediaInterpreterWrongDataType() {

        var data: Data?
        let response = HTTPURLResponse(url: URL(string: "test")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let successStatusCode = 200

        let photo1 = Photo(type: "video", comments: Count(count: 2), likes: Count(count: 3), images: Images(thumbnail: Image(url: "thumbnail_1"), low_resolution: Image(url: "low_resolution_1"), standard_resolution: Image(url: "standard_resolution_1")))
        let photo2 = Photo(type: "video", comments: Count(count: 1), likes: Count(count: 1), images: Images(thumbnail: Image(url: "thumbnail_2"), low_resolution: Image(url: "low_resolution_2"), standard_resolution: Image(url: "standard_resolution_2")))
        let photo3 = Photo(type: "video", comments: Count(count: 8), likes: Count(count: 8), images: Images(thumbnail: Image(url: "thumbnail_3"), low_resolution: Image(url: "low_resolution3"), standard_resolution: Image(url: "standard_resolution_3")))
        let photos = Photos(data: [photo1, photo2, photo3])

        do {
            data = try JSONEncoder().encode(photos)
        }
        catch {
            XCTFail()
        }

        let recentMediaInterpreter = RecentMediaInterpreter()
        let resp = recentMediaInterpreter.interpret(data: data, response: response, error: nil, successStatusCode: successStatusCode)
        switch resp {
        case .success(let data):
            XCTAssertEqual(data, Photos(data: []))
        case .error(_):
            XCTFail()
        }
    }

    func testRecentMediaInterpreterInvalidResponse() {

        let response = HTTPURLResponse(url: URL(string: "test")!, statusCode: 400, httpVersion: nil, headerFields: nil)
        let successStatusCode = 400
        let recentMediaInterpreter = RecentMediaInterpreter()
        let resp = recentMediaInterpreter.interpret(data: nil, response: response, error: nil, successStatusCode: successStatusCode)
        switch resp {
        case .success(_):
            XCTFail()
        case .error(let responseError):
            XCTAssertEqual(responseError, ResponseError.invalidResponseError)
        }
    }

    func testRecentMediaInterpreterError() {

        let response = HTTPURLResponse()
        let error = NSError(domain: "test", code: 100, userInfo: nil)
        let successStatusCode = 400

        let recentMediaInterpreter = RecentMediaInterpreter()
        let resp = recentMediaInterpreter.interpret(data: nil, response: response, error: error, successStatusCode: successStatusCode)
        switch resp {
        case .success(_):
            XCTFail()
        case .error(let responseError):
            XCTAssertEqual(responseError, ResponseError.connectionError)
        }
    }
    
}
