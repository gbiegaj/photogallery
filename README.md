# PhotoGallery - demo project

Created by Grzegorz Biegaj (17  - 27 June)

## Project description

Simple Shop is just an iOS demo app presenting clean code and application architecture.
The main feature of the app is to implement a photo gallery application using Instagram API, populated with photos, published on instagram by the user that logged in from the app.

### How it works:
- App implemens Instagram’s server-side (Explicit) login flow
- After successful login, user will see recent photos in collection view
- After selecting any photo it will be opend in full screen mode
- In full screen mode user can also scroll photos by swiping left / right
- All the photos (thumbnail and full screen) are cached locally in the disk storage
- User can also press "Logout" button to login to another Instagram account
- To refresh photos, user can try to use "pull to refresh" feature 

## Architecture

### Modified MVC
Because of usage storyboards introduction of pure MVVM is not so easy, because view is integrated with ViewControllers. Instead of it View Controller is separated by ViewModel and Controller

### Storyboards
For that simple app storyboards are good solution. Storyboards are split to possible small scenes accordingly to the ViewControllers

### Unit tests
Most of the possible unit tests exists

### Dependency injection
Because of usage unit tests it was necessary to introduce dependecy injection to separate components.

### Dependencies
Cocoapods is used as a dependency manager.

* [Locksmith](https://github.com/matthewpalmer/Locksmith) - keychain storage
* [Kingfisher](https://github.com/onevcat/Kingfisher) - library for downloading and caching images from the web

### Programming tools
Xcode 9.3, swift 4.1
